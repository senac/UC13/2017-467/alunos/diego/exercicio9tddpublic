/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.senac.model;


public class Automovel {
    
    private final double tempo;
    private final double velocidade;
    private double distancia;
    
    private final double consumoKMlitro = 12;
    private double consumoLitros;
    
    public Automovel(double tempoH, double velH){
        this.tempo = tempoH;
        this.velocidade = velH;
    }
    
    public double distanciaPercorridaCalc(){
        this.distancia = this.tempo * this.velocidade;
        return this.distancia;
    }
    
    public double consumoLitrosCalc(){
        this.consumoLitros = distanciaPercorridaCalc() / consumoLitros;
        return this.consumoLitros;
    }


}
