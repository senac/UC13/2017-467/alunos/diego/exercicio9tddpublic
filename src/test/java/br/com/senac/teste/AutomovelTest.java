/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.teste;

import br.com.senac.model.Automovel;
import org.junit.Test;
import static org.junit.Assert.*;


public class AutomovelTest {
    
    public AutomovelTest() {
    }
    
    @Test
    public void DistanciaCalc(){
        Automovel auto = new Automovel(25, 68);
        assertEquals(1700,auto.distanciaPercorridaCalc(), 0.01);
    }
}
